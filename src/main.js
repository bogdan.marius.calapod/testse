import Vue from 'vue'
import App from './App.vue'

import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  Vue,
  dsn: "https://af7e4cf787ce49d28b09a08b78ef9525@o487547.ingest.sentry.io/5546429",
  integrations: [
      new Integrations.BrowserTracing(),
    ],

     tracesSampleRate: 1.0,
     });

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
